# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# The following guidelines are specific to BZR, GIT, HG and SVN packages.
# Other VCS sources are not natively supported by makepkg yet.

# Maintainer: Matt Coffin <mcoffin13@gmail.com>
pkgname=exa
pkgver=0.9.0
pkgrel=2
pkgdesc="ls replacement"
arch=('x86_64')
url="http://the.exa.website"
license=('MIT')
groups=()
depends=(libssh2 zlib libgit2)
makedepends=(cargo cmake git rust) # 'bzr', 'git', 'mercurial' or 'subversion'
replaces=()
backup=()
options=()
install=
source=("${pkgname%-git}::git+https://github.com/ogham/exa.git#tag=v$pkgver"
        "0001-test-Fix-error-message-mismatch.patch")
noextract=()
sha512sums=('SKIP'
            '70e52c792f3afb3bdd9f0d4e96a951c1da5873c6a0f5e5496edb487a16892fe3f1b429308e0320496eb58815f1afd66e80818f4876536f65d0959085385b0733')

# Please refer to the 'USING VCS SOURCES' section of the PKGBUILD man page for
# a description of each element in the source array.

prepare() {
	cd "$srcdir/${pkgname%-git}"
	local _srcfile
	for _srcfile in "${source[@]}"; do
		_srcfile="${_srcfile%%::*}"
		_srcfile="${_srcfile##*/}"
		[[ $_srcfile = *.patch ]] || continue
		msg2 "Applying patch $_srcfile"
		patch -Np1 < "../$_srcfile"
	done
}

build() {
	cd "$srcdir/${pkgname%-git}"
	LIBGIT2_SYS_USE_PKG_CONFIG=1 \
		cargo build \
		--release
}

check() {
	cd "$srcdir/${pkgname%-git}"
	LIBGIT2_SYS_USE_PKG_CONFIG=1 \
		cargo test \
		--release
}

package() {
	make \
		-C "$srcdir/${pkgname%-git}" \
		DESTDIR="$pkgdir/" \
		PREFIX="/usr" \
		install
	cd "$srcdir/$pkgname"
	install -Dm644 contrib/completions.bash \
		"$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm644 contrib/completions.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_$pkgname"
	install -Dm644 contrib/completions.fish \
		"$pkgdir/usr/share/fish/vendor_completions.d/$pkgname.fish"
	install -Dm644 LICEN?E \
		"$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
